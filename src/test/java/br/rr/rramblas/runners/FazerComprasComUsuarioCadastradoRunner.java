package br.rr.rramblas.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features/FazerComprasComUsuarioCadastrado.feature",
		glue = "br.rr.rramblas.steps",
		//tags = {},
		plugin = {"pretty"},
		monochrome = true,
		snippets = SnippetType.CAMELCASE,
		dryRun = false
		)
public class FazerComprasComUsuarioCadastradoRunner {

}