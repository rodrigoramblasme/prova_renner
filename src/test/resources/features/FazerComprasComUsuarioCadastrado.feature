#language: pt
#encoding: utf-8

@cadastro_de_usuarios
Funcionalidade: Fazer Compras Com Usuario Cadastrado
  	Como um cliente cadastrado no automationpractice.com
  	Eu quero fazer a compra de ao menos 2 produtos
  	Para que eu possa estar bem vestida

	Contexto: 
		Dado que realizo o acesso ao site do desafio
	
	@cadastro_de_um_novo_usuario		
  Cenario: Cadastrar Novo Usuario		
  Quando clico em Sign in
    E informo o e-mail para criar nova conta
    E clico no botao Create an account
    E preencho as informacoes pessoais
    E preencho o endereco
    E clico no botao Register
    Entao visualizo a mensagem "Welcome to your account. Here you can manage all of your personal information and orders."
  
  @compras_com_usuario_cadastrado  
  Cenario: Fazer Compras Com Usuario Cadastrado
  	Quando clico em Sign in
  	E informo minhas credenciais de acesso
  	Entao visualizo a mensagem "Welcome to your account. Here you can manage all of your personal information and orders."
  	E pesquiso o produto "Printed Chiffon Dress"
  	E tamanho "M"
  	E seleciono a cor "verde"
  	E clico no botao Add to cart
  	E clico no botao Continue shopping
  	E pesquiso o produto "Faded Short Sleeve T-shirts"
  	E seleciono a cor "azul"
  	E clico no botao Add to cart
  	E clico no botao Continue shopping
  	E pesquiso o produto "Blouse"
  	E adiciono a quantidade "2"
  	E clico no botao Add to cart
  	E clico no botao Continue shopping
  	E pesquiso o produto "Printed Dress"
  	E adiciono a quantidade "1"
  	E clico no botao Add to cart
  	E clico no link Proceed to checkout
  	E clico no botao Proceed to checkout
  	E clico no botao Proceed to checkout
  	E marco que aceito todos os termos
  	E seleciono o metodo de pagamento "Pay by bank wire"
  	E clico no botao I confirm my order
  	Entao visualizo a mensagem de sucesso "Your order on My Store is complete."
  	