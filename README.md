# PROVA RENNER
[![NPM](https://img.shields.io/npm/l/react)](https://gitlab.com/rodrigoramblasme/prova_renner/-/blob/main/LICENSE)

## Sobre o projeto
Projeto de automação em Java, com Cucumber, API, objetos de página e relatórios.

## Ambiente
**1. Downloads do Java SE Development Kit 8**

O JDK é um ambiente de desenvolvimento para a construção de aplicativos, miniaplicativos e componentes usando a linguagem de programação Java.
O JDK inclui ferramentas úteis para desenvolver e testar programas escritos na linguagem de programação Java e executados na plataforma Java.
https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

**2. Colocar o Java no Path no Windows 10**
- Clicar no icone do windows pesquisar
- Pesquisar por "Editar as variáveis de ambiente do sistema"
- Clicar em "Variáveis de Ambiente"
- Em Variáveis do sistema, clicar em "NOVO"
- Colocar o Nome da váriavel JAVA_HOME e Valor da Variável C:\Program Files (x86)\Java\jre1.8.0_301
- Clicar em Path > Editar > Novo e colocar %JAVA_HOME%\bin

**3. Baixar a IDE Eclipse**

https://www.eclipse.org/downloads/ 

**4. Clocar o projeto**

git clone https://gitlab.com/rodrigoramblasme/prova_renner.git





















